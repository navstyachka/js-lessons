var room = {
  plants: {
    amount: 4,
    names: [ 'plant_1', 'plant_2', 'plant_3', 'plant_4' ]
  },
  posters: [
    { name:'poster_1', size:'small', colour:'white' },
    { name:'poster_2', size:'medium', colour:'green' },
    { name:'book', size:'big', colour:'black' }
  ],
  furniture: [
    { type:'shelf', id:0 },
    { type:'shelf', id:1 },
    { type:'table', id:2 },
    { type:'chair', id:3 }
  ]
}

console.log( 'Second plant: ', room.plants.names[1] )

console.log( room.posters[2].name )
