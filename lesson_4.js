var count = 100
count.toString()

var perfume = [
  {
    name: 'Clean Skin',
    producer: 'Demeter',
    price: 25.0,
    amount: 30,
    isRunOut: false,
    isGonnaBuyAgain: false
  },
  {
    name: 'Stone',
    producer: 'Masaki Matsushima',
    price: 45.0,
    amount: 100,
    isRunOut: true,
    isGonnaBuyAgain: true
  },
  {
    name: 'Le Jardin de Monsier Li',
    producer: 'Hermes',
    price: 45.0,
    amount: 30,
    isRunOut: true,
    isGonnaBuyAgain: false
  }
]

perfume.push({
      name: 'Lazy Sunday Morning',
      producer: 'Maison Martin Margela',
      price: 120.0,
      amount: 100,
      isRunOut: false,
      isGonnaBuyAgain: true
    })

// console.log(perfume,perfume.length)

var names = [ 'Anna', 'Angela', 'Amanda', 'Peter', 'John' ]
// console.log(names.indexOf('Amanda'))


var care = [
  'micellar water', 'thermal water', 'tonic', 'extraordinary water',
  'protecting emulsion', 'eye care roll-on', 'oil', 'lip balm'
]

// console.log('Lenght: ', care.length, care)
// console.log('Is There Water? ', care.indexOf('thermal water'))
// console.log('Sorted array ', care.sort())


//faceCare isGonnaBuyAgain

var unknownCategory = 'faceCare'

var cosmetics = {
  perfume:[
    {
      name: 'Clean Skin',
      producer: 'Demeter',
      price: 25.0,
      amount: 30,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      name: 'Stone',
      producer: 'Masaki Matsushima',
      price: 45.0,
      amount: 100,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      name: 'Le Jardin de Monsier Li',
      producer: 'Hermes',
      price: 45.0,
      amount: 30,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      name: 'Twilly',
      producer: 'Hermes',
      price: 68.0,
      amount: 50,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      name: 'Noa',
      producer: 'Cacharel',
      price: 25.0,
      amount: 50,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      name: 'Lazy Sunday Morning',
      producer: 'Maison Martin Margela',
      price: 120.0,
      amount: 100,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
  ],
  hair:[
    {
      type: 'shampoo',
      producer: 'Indola',
      price: 13.0,
      amount: 300,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'conditioner',
      producer: 'Lisap',
      price: 15.0,
      amount: 250,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      type: 'hydrating conditioner',
      producer: 'Insight',
      price: 19.0,
      amount: 500,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'hydrating mask',
      producer: 'Insight',
      price: 21.0,
      amount: 250,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'ceratin complex',
      producer: 'CHI',
      price: 16.0,
      amount: 59,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      type: 'sea salt spray',
      producer: 'NO INHIBITION',
      price: 16.5,
      amount: 250,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'hair shield',
      producer: 'Davines',
      price: 18.0,
      amount: 250,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      type: 'heat protection',
      producer: 'Karal',
      price: 12.0,
      amount: 115,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'dry shampoo',
      producer: 'Batiste',
      price: 8.0,
      amount: 200,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'dry shampoo',
      producer: 'Batiste',
      price: 8.0,
      amount: 200,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
  ],
  body:[
    {
      type: 'soap',
      producer: 'Yves Rocher',
      price: 3.5,
      amount: 200,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'shower gel',
      producer: 'Yves Rocher',
      price: 7.5,
      amount: 400,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'scrub',
      producer: 'Yves Rocher',
      price: 7.0,
      amount: 150,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      type: 'oil',
      producer: 'Yves Rocher',
      price: 4.8,
      amount: 150,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      type: 'body cream',
      producer: 'Yves Rocher',
      price: 2.8,
      amount: 200,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'body balm',
      producer: 'Yves Rocher',
      price: 4.5,
      amount: 390,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'body balm',
      producer: 'Yves Rocher',
      price: 4.5,
      amount: 390,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
  ],
  hands:[
    {
      type: 'cream',
      producer: 'Yves Rocher',
      price: 2.9,
      amount: 75,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      type: 'cream',
      producer: 'Yves Rocher',
      price: 1.9,
      amount: 30,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'scrub',
      producer: 'Yves Rocher',
      price: 3.9,
      amount: 75,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
  ],
  nails:[
    {
      colour: 'Yodel Me On My Cell',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: 'I Saw... U Saw... We Saw... Warsaw',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      colour: 'Chistmas Gone Plaid',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      colour: 'Dark Side Of The Moon',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      colour: 'My Car Has Navy-Gation',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      colour: 'A-Taupe The Space Needle',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Chick Fluck Cherry',
      producer: 'OPI',
      price: 3.25,
      amount: 15.0,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      colour: 'Re-Fresh Mint',
      producer: 'China Glaze',
      price: 2.50,
      amount: 14.0,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Lets groove ',
      producer: 'China Glaze',
      price: 2.50,
      amount: 14.0,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Greige',
      producer: 'Sally Hansen',
      price: 1.95,
      amount: 14.7,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      colour: 'Peachy Keen',
      producer: 'Sally Hansen',
      price: 1.95,
      amount: 14.7,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Birthday Suit',
      producer: 'Sally Hansen',
      price: 1.95,
      amount: 14.7,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      colour: '304',
      producer: 'LSN',
      price: 2.65,
      amount: 16.0,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      colour: '340',
      producer: 'LSN',
      price: 2.65,
      amount: 16.0,
      isRunOut: true,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Coffee With Milk',
      producer: 'inm',
      price: 1.40,
      amount: 15.0,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      colour: 'Java Va Voom!',
      producer: 'Cuccio',
      price: 3.10,
      amount: 13.0,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Pompeii It Forward',
      producer: 'Cuccio',
      price: 3.10,
      amount: 13.0,
      isRunOut: true,
      isGonnaBuyAgain: true
    }
  ],
  faceCare:[
    {
      type: 'scrub',
      producer: 'Yves Rocher',
      price: 4.8,
      amount: 50,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'mask',
      producer: 'Yves Rocher',
      price: 3.5,
      amount: 50,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'thermal spring water',
      producer: 'Avene',
      price: 5.0,
      amount: 150,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'mousturizing cream',
      producer: 'Avene',
      price: 25.0,
      amount: 40,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'micellar water',
      producer: 'Garnier',
      price: 7.0,
      amount: 400,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'tonic',
      producer: 'Yves Rocher',
      price: 10.0,
      amount: 200,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      type: 'extraordinary water',
      producer: 'Melvita',
      price: 27.0,
      amount: 100,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'eye care roll-on',
      producer: 'Yves Rocher',
      price: 19.5,
      amount: 15,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'lip balm',
      producer: 'Carmex',
      price: 6.0,
      amount: 4.9,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'face treatment oil',
      producer: 'Clarins',
      price: 34.0,
      amount: 30,
      isRunOut: false,
      isGonnaBuyAgain: true
    }
  ],
  decorativeCosmetic:[
    {
      type: 'BB cream',
      producer: 'Melvita',
      price: 27.0,
      amount: 40,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'consiler',
      producer: 'Yves Saint Laurent',
      price: 25.5,
      amount: 2.5,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'powder',
      producer: 'Clinique',
      price: 26.0,
      amount: 200,
      isRunOut: true,
      isGonnaBuyAgain: true
    },
    {
      type: 'gel for eyebrows',
      producer: 'Relouis',
      price: 3.0,
      amount: 7.5,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'highlighter',
      producer: 'Inglot',
      price: 8.0,
      amount: 7.0,
      isRunOut: false,
      isGonnaBuyAgain: true
    },
    {
      type: 'white pencil',
      producer: 'Yves Rosher',
      price: 2.3,
      amount: 3,
      isRunOut: false,
      isGonnaBuyAgain: true
    }
  ],
  lipstics:[
    {
      colour: 'Rouge Vif',
      producer: 'Yves Rosher',
      price: 5.0,
      amount: 3.7,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Violet Profond',
      producer: 'Yves Rosher',
      price: 5.0,
      amount: 3.7,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: 'Twig',
      producer: 'M.A.C.',
      price: 5.5,
      amount: 3.5,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
    {
      colour: '18',
      producer: 'Lux Visage',
      price: 2.8,
      amount: 4.0,
      isRunOut: false,
      isGonnaBuyAgain: false
    },
  ]
}

var categories = Object.keys(cosmetics)
// console.log(cosmetics[ categories[2] ])
// console.log(categories.indexOf('faceCare'))
var category = categories[ categories.indexOf(unknownCategory) ]
// console.log(cosmetics[ category ])
var index = cosmetics[ category ].length - 1
console.log('Is Gonna Buy Again? ',cosmetics[category][index].isGonnaBuyAgain)
