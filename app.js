// String
'text'
"text"
"a'a"
'a"a'
"a'a\"a"
'a"a\'a'
'I am \na new line'

// Number
10 // Primitive Value
var count = 100
console.log(count)

var count2 = new Number(100) // Number() — конструктор
console.log(count2)


// Boolean
true
false

// Null
null

// Undefined
undefined

/** Object
Format:
  {key: value, 'key': value}
Keys:
  Can NOT use as keys: key, null, false... etc зарезервированные слова
  Do: {'a-a': 1}, Don't: {a-a: 1}
**/


// 1 вариант Добавления контента в объект
var product = {} //empty
console.log(0, product)
product.price = 25
console.log(1, product)
product.coffee = {}
console.log(2, product)
product.coffee['tree-type'] = 'Arabica'
console.log(3, product)
product.coffee.height_value = 1800
console.log(4, product)

// 2 вариант Добавления контента в объект
var product2 = {
  coffee: {
    'tree-type': 'Arabica',
    type: 'Typica',
    height_value: 1800
  },
  price: 25,
  seller: {
    name: 'Kitchen',
    site: 'Kcr.by',
    address: {
      city: 'Minsk',
      line: 'Kastrychnickaya 10b'
    }
  },
  amounts: [100, 250, 1000],
  someMethod: function() { // это метод,т.е. функция
    console.log(1)
  }
}

//var product2 = {coffee: {type: 'Robusta'}, price: 30, amounts: [2000]}

console.log('Adress:', product2.seller.address.line, '\n')
console.log('Coffee Tree Type:', product2.coffee['tree-type'], '\n')

// Array
/**
Format:
  [1, 'sfdsdf', {}]
**/

var salad = ['cucmber', 'olive oil', 'tomatoes', 'salt']

console.log(salad[0]) // index starts with 0

/**
  If/Else Clauses.
Format:
  if (clause) {action}
  else if (clause) {action}
  else {action}
**/

// Simple clause with 1 clause
var items = ['a', 'b', 'c', 'd']
if (items.length < 10) {
  items.push('e')
}
console.log(items)

// If or Else clause
var isEnough
if (items.length < 10) {
  isEnough = false
} else {
  isEnough = true
}
console.log(isEnough)

// Clause with more than one clauses
if (items.length < 10) {
  isEnough = 'Not enough! :('
} else if (items.length > 10 ) {
  isEnough = 'I guess that\'s too much!'
} else {
  isEnough = 'Enough!'
}
